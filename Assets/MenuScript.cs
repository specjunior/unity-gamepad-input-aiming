﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    #region variables

    public GameObject mainMenu;
    public GameObject objects;  //inne obiekty w UI, ktore chcemy ukryć
    public PlayerController playerController;
    private float mouseSensitivity;
    [SerializeField] private Slider slider;
    [SerializeField] private Text sliderValue;

    #endregion
    #region InputSystem

    public void menuBtn(InputAction.CallbackContext context)
    {
        if (context.started)
            if (mainMenu.activeSelf) HideMenu();
            else ShowMenu();
    }

    #endregion

    private void Start()
    {
        HideMenu();
        mouseSensitivity = playerController.GetSensitivity();
        slider.value = mouseSensitivity;
        sliderValue.text = mouseSensitivity.ToString();
    }

    public void ChangeSensitivity(float value)
    {
        playerController.ChangeSensitivityFromOptions(value);
        sliderValue.text = value.ToString();
    }

    public void ShowMenu()
    {
        mainMenu.SetActive(true);
        objects.SetActive(false);
        Cursor.visible = true;
        Time.timeScale = 0;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(slider.gameObject);
    }

    public void HideMenu()
    {
        mainMenu.SetActive(false);
        objects.SetActive(true);
        Time.timeScale = 1;
        Cursor.visible = false;
    }
}
