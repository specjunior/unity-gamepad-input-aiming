﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInputStuff : MonoBehaviour
{
    #region variables

    [SerializeField]
    private bool visibility = false;
    public bool moveLeftRight=false;

    public float delta = 1.5f;
    public float speed = 2.0f;
    private Vector3 startPos;

    #endregion
    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        if(moveLeftRight)
        {
            Vector3 v = startPos;
            v.z += delta * Mathf.Sin(Time.time * speed);
            transform.position = v;
        }        
    }
    public bool isVisible()
    {
        return visibility;
    }
    void OnBecameVisible()
    {
        visibility = true;
    }

    void OnBecameInvisible()
    {
        visibility = false;
    }
}
