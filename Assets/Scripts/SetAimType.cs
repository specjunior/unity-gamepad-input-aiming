﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SetAimType : MonoBehaviour
{
    [SerializeField] AimScript aimScript;
    [SerializeField] private GameObject[] UiIndicators;
    [SerializeField] private int current_aim = 0;

    #region InputSystem
    public void NextAimBind(InputAction.CallbackContext context)
    {
        if (context.started && Time.timeScale != 0)
            NextAim();
    }
    public void PreviousAimBind(InputAction.CallbackContext context)
    {
        if (context.started && Time.timeScale != 0)
            PreviousAim();
    }

    #endregion
    private void Awake()
    {
        SetAim(current_aim);
    }
    void SetAim(int targetState)
    {
        UiIndicators[current_aim].GetComponent<Animator>().SetTrigger("Trigger");
        current_aim = targetState;
        UiIndicators[current_aim].GetComponent<Animator>().SetTrigger("Trigger");
        aimScript.SetAimType(current_aim);
    }
    public void NextAim()
    {
        
        UiIndicators[current_aim].GetComponent<Animator>().SetTrigger("Trigger");
        if (current_aim == UiIndicators.Length - 1) current_aim = 0;
        else current_aim += 1;
        UiIndicators[current_aim].GetComponent<Animator>().SetTrigger("Trigger");
        aimScript.SetAimType(current_aim);
    }
    public void PreviousAim()
    {
        UiIndicators[current_aim].GetComponent<Animator>().SetTrigger("Trigger");
        if (current_aim == 0) current_aim = 3;
        else current_aim -= 1;
        UiIndicators[current_aim].GetComponent<Animator>().SetTrigger("Trigger");
        aimScript.SetAimType(current_aim);
    }
}
