﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    #region variables

    private Rigidbody rb;
    private Vector2 MoveInput;
    private Vector2 LookInput;
    private float playerIsJumping;
    private float isAiming;
    private float rightBumper;
    private float leftBumper;
    private float myszGoraDol = 0.0f, myszLewoPrawo = 0.0f;
    private int startedAim = 0;

    public float jumpForce;
    public ForceMode forceMode;
    public float moveSpeed = 9.0f;
    public float runSpeed = 7.0f;
    public float distToGround=0f;

    public float mouseSensitivity = 3.0f;
    private float mouseSensitivity_temp ;
    public float upDownLookRange = 90.0f;

    [SerializeField] private bool walk, run, crouch, jump;
    public Transform body;
    public GameObject firstPersonCam;
    public CapsuleCollider capsuleCollider;
    #endregion

    #region InputSystem
    public void OnMove(InputAction.CallbackContext context)
    {
        MoveInput = context.ReadValue<Vector2>();

    }
    public void OnLook(InputAction.CallbackContext context)
    {
        LookInput = context.ReadValue<Vector2>();
        
    }
    public void OnJump(InputAction.CallbackContext context)
    {
        playerIsJumping = context.ReadValue<float>();
        if (playerIsJumping == 1 && IsGrounded() && Time.timeScale != 0) rb.AddForce(jumpForce * rb.mass * Time.timeScale * Vector3.up, forceMode);
    }

    public void OnAim(InputAction.CallbackContext context)
    {
        if (context.started) startedAim = 1;
        if (context.canceled) mouseSensitivity = mouseSensitivity_temp;
            isAiming = context.ReadValue<float>();
    }
    public void RightBumper(InputAction.CallbackContext context)
    {
        if (context.started)
        rightBumper = context.ReadValue<float>();
    }
    public void LeftBumper(InputAction.CallbackContext context)
    {
        if (context.started)
            leftBumper = context.ReadValue<float>();
    }

    #endregion

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        distToGround = capsuleCollider.bounds.extents.y;
        mouseSensitivity_temp = mouseSensitivity;
    }
    void Update()
    {
        if (Time.timeScale == 0) return;
        klawiatura();
        if(isAiming==1)
        {
            GetComponent<AimScript>().Aim(LookInput != Vector2.zero, MoveInput != Vector2.zero,leftBumper==1 && rightBumper==1?0:leftBumper==1?-1:rightBumper==1?1:0, startedAim);

            if (LookInput!=Vector2.zero) myszka();
            else if(GetComponent<AimScript>().GetAimType()!=0 && 
                GetComponent<AimScript>().TargetIsreachable())
            {
                //gdy skończymy celować, to ustawiamy wartoci, aby kursor się nie przesunął w ostatnią pozycję zanim celowaliśmy
                float temp = firstPersonCam.transform.rotation.eulerAngles.x;
                myszLewoPrawo = 0; myszGoraDol = temp > 90 ? temp - 360 : temp;
            }
            startedAim = 0; //potrzebne, żebywykryć kiedy wcisnęliśmy przycisk celowania
        }
        else
        {
            myszka();
        }
        leftBumper = 0; rightBumper = 0; //potrzebne, żebywykryć kiedy wcisnęliśmy zmianę celu
    }

    private void klawiatura()
    {
        rb.MovePosition(transform.position + Time.timeScale*0.1F*moveSpeed/2 * transform.TransformDirection(MoveInput.x, 0, MoveInput.y));
        
    }

    private void myszka()
    {

        myszLewoPrawo = LookInput.x* mouseSensitivity*0.5f;
        transform.Rotate(0, myszLewoPrawo, 0);
        myszGoraDol -= LookInput.y* mouseSensitivity * 0.5f;

        //Funkcja nie pozwala aby wartość przekroczyła dane zakresy.
        myszGoraDol = Mathf.Clamp(myszGoraDol, -upDownLookRange, upDownLookRange);
        firstPersonCam.transform.localRotation = Quaternion.Euler(myszGoraDol, 0, 0);

    }
    bool IsGrounded(){
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1F);
    }

    public float GetSensitivity()
    {
        return mouseSensitivity_temp;
    }
    public void ChangeSensitivity(float value)
    {
        mouseSensitivity = value;
    }
    public void ChangeSensitivityFromOptions(float value)
    {
        if (mouseSensitivity == mouseSensitivity_temp) mouseSensitivity = value;
        mouseSensitivity_temp = value;
    }
    //przywraca początkową wartość czułości
    public void ResetSensitivity()
    {
        mouseSensitivity = mouseSensitivity_temp;
    }
}
